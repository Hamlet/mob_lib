--[[
	Mobiles Library - Provides functions for LUA entities.
	Copyright © 2019 Hamlet <hamlatmesehub@riseup.net> and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Variables
--

-- Used for localization, choose either built-in or intllib.

local s_ModPath, S, NS = nil

if (minetest.get_modpath("intllib") == nil) then
	S = minetest.get_translator("mob_lib")

else
	-- internationalization boilerplate
	s_ModPath = minetest.get_modpath("mob_lib")
	S, NS = dofile(s_ModPath.."/intllib.lua")

end


-- Used by Mobs' Redo
local i_MobDifficulty = minetest.settings:get("mob_difficulty")
if (i_MobDifficulty == nil) then
	i_MobDifficulty = 1
end


--
-- Functions.
--

-- Allows to set a damage value that reflects in-game times' real time.
-- a_s_Element: either 'water' or 'lava'
mob_lib.ElementalDamagePerSecond = function(self, a_s_Element)
	local i_SECONDS_IN_A_DAY = 86400
	local i_SECONDS_IN_5_MINUTES = 300
	local i_SECONDS_CONSTANT = 100 -- 1 third of the former.
	local i_DamageSpeed = nil
	local i_HitPoints = nil
	local i_TimeSpeed = nil
	local f_InGameDayLength = nil
	local f_FiveInGameMinutes = nil
	local f_DamagePerSecond = nil

	i_HitPoints = self.health
	i_TimeSpeed = minetest.settings:get("time_speed")

	if (i_TimeSpeed == nil) then
		i_TimeSpeed = 72 -- Default Minetest Game's time speed.

	elseif (i_TimeSpeed == 0) then
		i_TimeSpeed = 1 -- Same speed as in the real world.
	end

	if (element == "water") then
		i_DamageSpeed = i_SECONDS_IN_5_MINUTES

	elseif (element == "lava") then
		i_DamageSpeed = i_SECONDS_CONSTANT
	end

	f_InGameDayLength = (i_SECONDS_IN_A_DAY / i_TimeSpeed)
	f_FiveInGameMinutes = ((f_InGameDayLength * i_DamageSpeed) / i_SECONDS_IN_A_DAY)
	f_DamagePerSecond = (i_HitPoints / f_FiveInGameMinutes)

	return f_DamagePerSecond
end


-- Adds the 'b_Hit' flag to the mob's properties if not present.
-- Adds the 'f_LastHitTime' flag to the mob's properties if not present.
-- To be used with mob_lib.Experience
-- Meant to be placed into mobile's do_punch function.
-- Uses the default Mobs Redo attack field.
mob_lib.HitFlag = function(self)
	if (self.b_Hit ~= true) then
		self.b_Hit = true
	end

	if (self.f_CooldownTimer ~= 10) then
		self.f_CooldownTimer = 10
		-- Seconds before gaining experience.
		-- Prevents mobs from gaining experience when hit by projectiles.
	end
end


-- Increases the mob's damage and or armor level.
-- To be used with:
-- mob_lib.HitFlag
-- mob_lib.SurvivedFlag
-- Uses the default Mobs Redo attack field.
mob_lib.Experience = function(self, dtime)

	-- Allows experience gain for mobs that have been directly hit.
	if (self.b_Hit == true) and (self.attack == nil) then
		if (self.f_CooldownTimer > 0) then
			self.f_CooldownTimer = (self.f_CooldownTimer - dtime)

		else
			self.b_Hit = false
			self.f_CooldownTimer = 10

			self.i_SurvivedFights = (self.i_SurvivedFights + 1)
			print(self.nametag.." survived "..self.i_SurvivedFights.." fights.")

			if (self.damage < (8 * i_MobDifficulty)) then
				self.damage = (self.damage + 1)
			end

			if (self.armor > 10) then
				self.armor = (self.armor - 1)

				self.object:set_armor_groups({fleshy = self.armor})
			end
		end
	end
end


-- Allows to heal over time.
-- To be used with mob_lib.HurtFlag
--[[
	FUNCTION'S ARGUMENTS
	====================

	self: the LUA entity.
	dtime: the server's time tick.
	a_t_states: table - the mob's states which allow healing, e.g. "stand".
	a_i_hp: integer - hit points recoverable per step, e.g. 2 or 1 MTG's heart.
	a_f_delay: float - time that must pass before recovering a_i_hp.
	a_i_max_hp: integer - limit that will stop the healing process.
--]]

mob_lib.Heal = function(self, dtime, a_t_states, a_i_hp, a_f_delay, a_i_max_hp)
	if (self.b_Hurt ~= nil) then
		if (self.f_HealTimer == nil) then
			self.f_HealTimer = a_f_delay
		end

		if (self.health == a_i_max_hp) then
			self.b_Hurt = false
		else
			self.b_Hurt = true
		end

		if (self.b_Hurt == true) then
			local b_CanHeal = false

			for i = 1, #a_t_states do
				if (self.state == a_t_states[i]) then
					b_CanHeal = true
				end
			end

			if (b_CanHeal == true) then
				if (self.health < a_i_max_hp) then
					if (self.f_HealTimer > 0) then
						self.f_HealTimer = (self.f_HealTimer - dtime)

					else
						self.f_HealTimer = a_f_delay
						self.health = (self.health + a_i_hp)

						if (self.health > a_i_max_hp) then
							local i_Excess = (self.health - a_i_max_hp)
							self.health = (self.health - i_Excess)
						end

						self.object:set_hp(self.health)
					end
				end
			end
		end
	end
end


-- Adds the 'b_Hurt' flag to the mob's properties if not present.
-- To be used with mob_lib.Heal
-- Meant to be placed into mobile's on_spawn function.
mob_lib.HurtFlag = function(self)
	if (self.b_Hurt == nil) then
		if (self.health == self.max_hp) then
			self.b_Hurt = false

		else
			self.b_Hurt = true
		end
	end
end


-- Allows to randomly choose an attack type.
-- These are the default used by Mobs Redo.
mob_lib.RandomAttackType = function()
	local i_Number = math.random(1, 3)
	local s_AttackName = nil

	if (i_Number == 1) then
		s_AttackName = 'dogfight'

	elseif (i_Number == 2) then
		s_AttackName = 'shoot'

	else
		s_AttackName = 'dogshoot'

	end

	return s_AttackName
end


-- Allows to randomly choose a mob type.
-- These are the default used by Mobs Redo.
mob_lib.RandomMobileType = function()
	local i_Number = math.random(1, 90)
	local s_Type = nil

	if (i_Number <= 30) then
		s_Type = 'animal'

	elseif (i_Number > 30) and (i_Number <= 60) then
		s_Type = 'monster'

	else
		s_Type = 'npc'

	end

	return s_Type
end


-- Returns a string containing the mob's stats.
mob_lib.StatsString = function(self)
	local s_Nametag = S('Name: ') .. self.nametag .. '\n'
	local s_MobType = S('Type: ') .. self.type .. '\n'
	local s_Armor = S('Armor: ') .. self.armor .. '\n'
	local s_Damage = S('Damage: ') .. self.damage

	local s_StatsString = s_Nametag .. s_MobType .. s_Armor .. s_Damage

	return s_StatsString
end


-- Adds the 'i_SurvivedFights' flag to the mob's properties if not present.
-- To be used with mob_lib.Experience
-- Meant to be placed into mobile's on_spawn function.
mob_lib.SurvivedFlag = function(self)
	if (self.i_SurvivedFights == nil) then
		self.i_SurvivedFights = 0
	end
end
