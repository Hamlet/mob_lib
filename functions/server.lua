--[[
	Mobiles Library - Provides functions for LUA entities.
	Copyright © 2019 Hamlet <hamlatmesehub@riseup.net> and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Local constants' aliases.
--

local i_TIME_K = 24000
local i_DAWN = 4700 -- ~4:45am
local i_DUSK = 19250 -- ~7:15pm
local i_HOURS_24 = 86400 -- (86400 seconds / 60) / 60
local i_MINUTES_20 = 1200 -- (1200 seconds / 60)


--
-- Functions
--

-- Returns True if daytime, False if not.
mob_lib.DayOrNight = function()
	local b_DayTime = false
	local i_Time = (minetest.get_timeofday() * i_TIME_K)

	if (i_Time >= i_DAWN) and (i_Time <= i_DUSK) then
		b_DayTime = true

	else
		b_DayTime = false

	end

	return b_DayTime
end


-- Converts a real time value to an in-game time value.
-- a_i_seconds: seconds to be converted.
mob_lib.RealTimeToInGameTime = function(a_i_seconds)
	local i_TimeSpeed = minetest.settings:get("time_speed")
	local i_InGameDayLenght = nil
	local f_InGameSeconds = nil

	if (i_TimeSpeed == nil) then
		-- Minetest Game's default i_time speed = 72
		i_InGameDayLenght = MINUTES_20

	elseif (time_speed == 0) then
		-- If i_time is set to 0 mobiles will behave as if it is set to 1.
		i_InGameDayLenght = 1

	else
		i_InGameDayLenght = (HOURS_24 / i_TimeSpeed)
	end

	f_InGameSeconds = ((i_InGameDayLenght * a_i_seconds) / HOURS_24)

	return f_InGameSeconds
end
