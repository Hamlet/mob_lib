--[[
	Mobiles Library - Provides functions for LUA entities.
	Copyright © 2019 Hamlet <hamlatmesehub@riseup.net> and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the 'Licence');
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	'AS IS' basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Functions
--

--[[
	Returns a table, to be used with mobs using the 3D amor's mesh.
	None of the arguments is mandatory, if 'nil' then a transparent
	texture will be used.
	Each argument's table must contain at least one element,
	that is a PNG image.
	Example: a_t_Skins = {'mod_name_skin1.png', 'mod_name_skin2.png'}
	Example: a_t_Weapons = {'mod_name_sword.png', 'mod_name_axe.png'}

	Argument's strings, or armors' strings, must use texture modifiers.
	Example:
	a_s_ArmorAndHelmetAndShield = 'mod_name_helmet.png' ..
		'^' .. 'mod_name_chestplate.png' ..
		'^' .. 'mod_name_leggings.png' ..
		'^' .. 'mod_name_boots.png' ..
		'^' .. 'mod_name_shield.png'

	The above example will create a full armor suit; each element can
	be omitted, the 'missing' parts will show the 'skin' texture.
--]]

mob_lib.t_RandomAppearence = function(a_t_Skins, a_t_Weapons, a_s_Armor,
	a_s_ArmorAndHelmet, a_s_ArmorAndShield, a_s_ArmorAndHelmetAndShield)

	local t_ChosenAppearence = {}
	local t_Armors = nil
	local s_ChosenSkin = 'mob_lib_transparent.png'
	local s_ChosenArmor = 'mob_lib_transparent.png'
	local s_ChosenWeapon = 'mob_lib_transparent.png'

	-- Buil the armors table.
	if (a_s_Armor ~= nil) then
		table.insert(t_Armors, a_s_Armor)
	end

	if (a_s_ArmorAndHelmet ~= nil) then
		table.insert(t_Armors, a_s_ArmorAndHelmet)
	end

	if (a_s_ArmorAndShield ~= nil) then
		table.insert(t_Armors, a_s_ArmorAndShield)
	end

	if (a_s_ArmorAndHelmetAndShield ~= nil) then
		table.insert(t_Armors, a_s_ArmorAndHelmetAndShield)
	end

	-- Choose a skin from the provided ones, if any.
	if (a_t_Skins ~= nil) then
		s_ChosenSkin = a_t_Skins[math.random(1, #a_t_Skins)]
	end

	-- Choose an armor suit from the provided ones, if any.
	if (t_Armors ~= nil) then
		s_ChosenArmor = t_Armors[math.random(1, #t_Armors)]
	end

	-- Choose a weapon from the provided ones, if any.
	if (a_t_Weapons ~= nil) then
		s_ChosenWeapon = a_t_Weapons[math.random(1, #a_t_Weapons)]
	end

	table.insert(t_ChosenAppearence, 1, s_ChosenSkin)
	table.insert(t_ChosenAppearence, 2, s_ChosenArmor)
	table.insert(t_ChosenAppearence, 3, s_ChosenWeapon)
	table.insert(t_ChosenAppearence, 4, 'mob_lib_transparent.png')

	return t_ChosenAppearence
end


-- Returns a random string, to be used as a name.
-- a_i_Length: integer
mob_lib.RandomString = function(a_i_Length)

	local i_Letter = 0
	local i_Number = 0
	local b_InitialLetter = true
	local s_String = ''
	local s_Exchanger = ''
	local s_ForcedChoice = ''
	local t_Vowels = {'a', 'e', 'i', 'o', 'u'}
	local t_Semivowels = {'y', 'w'}

	local t_SimpleConsonants = {
		'm', 'n', 'b', 'p', 'd', 't', 'g', 'k', 'l', 'r', 's', 'z', 'h'
	}

	local t_CompoundConsonants = {
		'ñ', 'v', 'f', 'ð', 'þ', 'ɣ', 'ħ', 'ɫ', 'ʃ', 'ʒ'
	}

	local t_CompoundConsonantsUppercase = {
		'Ñ', 'V', 'F', 'Ð', 'Þ', 'Ɣ', 'Ħ', 'Ɫ', 'Ʃ', 'Ʒ'
	}

	local t_DoubleConsonants = {
		'mm', 'mb', 'mp', 'mr', 'ms', 'mz', 'mf',
		'mʃ',
		'nn', 'nd', 'nt', 'ng', 'nk', 'nr', 'ns', 'nz',
		'nð', 'nþ', 'nɣ', 'nħ', 'nʃ', 'nʒ',
		'bb', 'bl', 'br', 'bz',
		'bʒ',
		'pp', 'pl', 'pr', 'ps',
		'pʃ',
		'dd', 'dl', 'dr', 'dz',
		'dʒ',
		'tt', 'tl', 'tr', 'ts',
		'tʃ',
		'gg', 'gl', 'gr', 'gz',
		'gʒ',
		'kk', 'kl', 'kr', 'ks',
		'kʃ',
		'll', 'lm', 'ln', 'lb', 'lp', 'ld', 'lt', 'lg', 'lk', 'ls', 'lz',
		'lñ', 'lv', 'lf', 'lð', 'lþ', 'lɣ', 'lħ', 'lʃ', 'lʒ',
		'rr', 'rm', 'rn', 'rb', 'rp', 'rd', 'rt', 'rg', 'rk', 'rs', 'rz',
		'rñ', 'rv', 'rf', 'rð', 'rþ', 'rɣ', 'rħ', 'rʃ', 'rʒ',
		'ss', 'sp', 'st', 'sk',
		'sf',
		'zz', 'zm', 'zn', 'zb', 'zd', 'zg', 'zl', 'zr',
		'zñ', 'zv',
		'vl', 'vr',
		'fl', 'fr',
		'ðl', 'ðr',
		'þl', 'þr',
		'ɣl', 'ɣr',
		'ħl', 'ħr',
		'ʃp', 'ʃt', 'ʃk',
		'ʃf',
		'ʒm', 'ʒn', 'ʒb', 'ʒd', 'ʒg', 'ʒl', 'ʒr',
		'ʒv'
	}

	local t_DoubleConsonantsUppercase = {
		'Bl', 'Br', 'Bz',
		'Bʒ',
		'Pl', 'Pr', 'Ps',
		'Pʃ',
		'Dl', 'Dr', 'Dz',
		'Dʒ',
		'Tl', 'Tr', 'Ts',
		'Tʃ',
		'Gl', 'Gr', 'Gz',
		'Gʒ',
		'Kl', 'Kr', 'Ks',
		'Kʃ',
		'Sp', 'St', 'Sk',
		'Sf',
		'Zm', 'Zn', 'Zb', 'Zd', 'Zg', 'Zl', 'Zr',
		'Zñ', 'Zv',
		'Vl', 'Vr',
		'Fl', 'Fr',
		'Ðl', 'Ðr',
		'Þl', 'Þr',
		'Ɣl', 'Ɣr',
		'Ħl', 'Ħr',
		'Ʃp', 'Ʃt', 'Ʃk',
		'Ʃf',
		'Ʒm', 'Ʒn', 'Ʒb', 'Ʒd', 'Ʒg', 'Ʒl', 'Ʒr',
		'Ʒv'
	}

	local s_PreviousLetter = ''

	for i_InitialValue = 1, a_i_Length do

		i_Letter = i_Letter + 1

		--[[
			1: vowel
			2: semivowel
			3: simple consonant
			4: compound consonant
			5: double consonant
		--]]
		local i_ChosenGroup = math.random(1, 5)

		-- Previously used group type
		if (s_Exchanger == 'vowel') then
			--[[
				3: simple consonant
				4: compound consonant
				5: double consonant
			--]]
			i_ChosenGroup = math.random(3, 5)

		elseif (s_Exchanger == 'semivowel') then
			-- 1: vowel
			i_ChosenGroup = 1

		elseif (s_Exchanger == 'simple consonant') then
			if (i_Letter < a_i_Length) then
				--[[
					1: vowel
					2: semivowel
				--]]
				i_ChosenGroup = math.random(1, 2)
			else
				-- Vowel
				i_ChosenGroup = 1
			end

		elseif (s_Exchanger == 'compound consonant') then
			-- Vowel
			i_ChosenGroup = 1

		elseif (s_Exchanger == 'double consonant') then
			-- Vowel
			i_ChosenGroup = 1

		end

		-- Vowels
		if (i_ChosenGroup == 1) then

			-- Uppercase vowel
			if (b_InitialLetter == true) then
				b_InitialLetter = false
				i_Number = math.random(1, #t_Vowels)
				s_PreviousLetter = string.upper(t_Vowels[i_Number])
				s_String = s_String .. s_PreviousLetter

			-- Lowercase vowel
			else
				i_Number = math.random(0, 1) -- single or double vowel

				if (i_Number == 0) then
					i_Number = math.random(1, #t_Vowels)
					s_PreviousLetter = t_Vowels[i_Number]
					s_String = s_String .. s_PreviousLetter

				else
					i_Number = math.random(1, #t_Vowels)
					s_PreviousLetter = t_Vowels[i_Number]
					s_String = s_String .. s_PreviousLetter

					i_Number = math.random(1, #t_Vowels)
					s_PreviousLetter = t_Vowels[i_Number]
					s_String = s_String .. s_PreviousLetter

				end
			end

			s_Exchanger = 'vowel'

		-- Semivowels
		elseif (i_ChosenGroup == 2) then

			i_Number = math.random(1, 2) -- single or double semivowel

			-- Uppercase semivowel
			if (i_Letter ~= 2) then
				if (b_InitialLetter == true) then
					b_InitialLetter = false
					s_PreviousLetter = string.upper(t_Semivowels[i_Number])
					s_String = s_String .. s_PreviousLetter

				-- Lowercase semivowel
				else
					s_PreviousLetter = t_Semivowels[i_Number]
					s_String = s_String .. s_PreviousLetter

				end

				s_Exchanger = 'semivowel'

			-- Lowercase semivowel
			elseif (i_Letter == 2) then
				if (s_PreviousLetter == 'L') or (s_PreviousLetter == 'R')
				or (s_PreviousLetter == 'Ɫ') or (s_PreviousLetter == 'Y')
				or (s_PreviousLetter == 'W') or (s_PreviousLetter == 'H')
				then
					if (i_Number == 1) then
						s_PreviousLetter = 'i'
						s_String = s_String .. s_PreviousLetter

					elseif (i_Number == 2) then
						s_PreviousLetter = 'u'
						s_String = s_String .. s_PreviousLetter

					end
				end

				s_Exchanger = 'vowel'
			end

		-- Simple consonants
		elseif (i_ChosenGroup == 3) then

			i_Number = math.random(1, #t_SimpleConsonants)

			-- Uppercase consonant
			if (b_InitialLetter == true) then
				b_InitialLetter = false
				s_PreviousLetter = string.upper(t_SimpleConsonants[i_Number])
				s_String = s_String .. s_PreviousLetter

			-- Lowercase consonant
			else
				s_PreviousLetter = t_SimpleConsonants[i_Number]
				s_String = s_String .. s_PreviousLetter

			end

			s_Exchanger = 'simple consonant'

		-- Compound consonants
		elseif (i_ChosenGroup == 4) then

			i_Number = math.random(1, #t_CompoundConsonantsUppercase)

			-- Uppercase compound consonant
			if (b_InitialLetter == true) then
				b_InitialLetter = false
				s_PreviousLetter = t_CompoundConsonantsUppercase[i_Number]
				s_String = s_String .. s_PreviousLetter

			-- Lowercase compound consonant
			else
				s_PreviousLetter = t_CompoundConsonants[i_Number]
				s_String = s_String .. s_PreviousLetter
			end

			s_Exchanger = 'compound consonant'

		-- Double consonants
		elseif (i_ChosenGroup == 5) then

			-- Uppercase double consonant
			if (b_InitialLetter == true) then
				b_InitialLetter = false
				i_Number = math.random(1, #t_DoubleConsonantsUppercase)
				s_PreviousLetter = t_DoubleConsonantsUppercase[i_Number]
				s_String = s_String .. s_PreviousLetter

			-- Lowercase double consonant
			else
				i_Number = math.random(1, #t_DoubleConsonants)
				s_PreviousLetter = t_DoubleConsonants[i_Number]
				s_String = s_String .. s_PreviousLetter
			end

			s_Exchanger = 'double consonant'

		end
	end

	b_InitialLetter = true

	return s_String
end


-- Returns True or False
mob_lib.RandomTrueOrFalse = function()
	if (math.random(0, 1) == 0) then
		return false
	else
		return true
	end
end


--[[
	Returns a table containing a pickaxe, its damage and its texture.
	If a_b_RealisticChance == true then ores percentages' are taken
	in account for the pickaxe choice.
	Table's elements:
	s_ItemString - example: 'default:pick_wood'
	i_Damage - example: 2
	s_Texture - example: 'default_tool_woodpick.png'
--]]

mob_lib.RandomPickaxe = function(a_b_RealisticChance)
	local i_RandomNumber = nil
	local t_ChosenPickaxe = {}

	local t_DefaultPickaxes = {
		{
			s_ItemString = 'default:pick_wood',
			i_Damage = 2,
			s_Texture = 'default_tool_woodpick.png'
		},

		{
			s_ItemString = 'default:pick_stone',
			i_Damage = 3,
			s_Texture = 'default_tool_stonepick.png'
		},

		{
			s_ItemString = 'default:pick_bronze',
			i_Damage = 4,
			s_Texture = 'default_tool_bronzepick.png'
		},

		{
			s_ItemString = 'default:pick_steel',
			i_Damage = 4,
			s_Texture = 'default_tool_steelpick.png'
		},

		{
			s_ItemString = 'default:pick_mese',
			i_Damage = 5,
			s_Texture = 'default_tool_mesepick.png'
		},

		{
			s_ItemString = 'default:pick_diamond',
			i_Damage = 5,
			s_Texture = 'default_tool_diamondpick.png'
		}
	}



	if (a_b_RealisticChance ~= true) then
		t_ChosenPickaxe = t_DefaultPickaxes[math.random(1, 6)]

	else
		--[[
			These percentages have been collected using Ores Stats
			Mapgen = flat
			Map seed = 0123456789
			Letting the character in autowalk from surface to -31000.

			The ore's percentages sum is 50.476%
			Stone and wood percentages have been arbitrarily set:
			(100% - 50.476%) = 49.524% -- To be assigned.
			(49.524% / 6) = 8.254% -- Allows the following:

			Stone's percentage = Twice the wood's percentage
			Stone is everywhere, wood might be harder to find.

			Stone = (8.254% * 4) = 33.016%
			Wood = (8.254% * 2) = 16.508%

			(33.016% + 16.508%) = 49.524% -- Assigned.

			Ores + (Wood + Stone) = 50.476% + 49.524% = 100% Assigned.
		--]]

		-- These values have been rounded to integers due the fact
		-- that the pseudorandom number generator only produces integers.

		local i_DIAMOND_ORE_PERCENTAGE = 2
		-- Actually 1.545

		local i_MESE_ORE_PERCENTAGE = 3
		-- Actually 2.818

		local i_COPPER_ORE_PERCENTAGE = 14
		-- Actually 13.796, used for 'bronze'.

		local i_WOOD_PERCENTAGE = 17
		-- Actually 16.508

		local i_IRON_ORE_PERCENTAGE = 32
		-- Actually 32.317, used for 'steel'.

		local i_STONE_PERCENTAGE = 33
		-- Actually 33.016

		i_RandomNumber = PseudoRandom(os.time())
		i_RandomNumber = i_RandomNumber:next(1, 100)

		if (i_RandomNumber <= i_DIAMOND_ORE_PERCENTAGE) then
			t_ChosenPickaxe = t_DefaultPickaxes[6] -- Diamond pickaxe.

		elseif (i_RandomNumber > i_DIAMOND_ORE_PERCENTAGE)
		and (i_RandomNumber <= i_MESE_ORE_PERCENTAGE)
		then
			t_ChosenPickaxe = t_DefaultPickaxes[5] -- Mese pickaxe.

		elseif (i_RandomNumber > i_MESE_ORE_PERCENTAGE)
		and (i_RandomNumber <= i_COPPER_ORE_PERCENTAGE)
		then
			t_ChosenPickaxe = t_DefaultPickaxes[3] -- Copper pickaxe.

		elseif (i_RandomNumber > i_COPPER_ORE_PERCENTAGE)
		and (i_RandomNumber <= i_WOOD_PERCENTAGE)
		then
			t_ChosenPickaxe = t_DefaultPickaxes[1] -- Wooden pickaxe.

		elseif (i_RandomNumber > i_WOOD_PERCENTAGE)
		and (i_RandomNumber <= i_IRON_ORE_PERCENTAGE)
		then
			t_ChosenPickaxe = t_DefaultPickaxes[4] -- Steel pickaxe.

		else
			t_ChosenPickaxe = t_DefaultPickaxes[2] -- Stone pickaxe.

		end
	end

	return t_ChosenPickaxe
end


--[[
	Returns a table containing a sword, its damage and its texture.
	If a_b_RealisticChance == true then ores percentages' are taken
	in account for the sword choice.
	Table's elements:
	s_ItemString - example: 'default:sword_wood'
	i_Damage - example: 2
	s_Texture - example: 'default_tool_woodsword.png'
--]]

mob_lib.RandomSword = function(a_b_RealisticChance)
	local i_RandomNumber = nil
	local t_ChosenSword = {}

	local t_DefaultSwords = {
		{
			s_ItemString = 'default:sword_wood',
			i_Damage = 2,
			s_Texture = 'default_tool_woodsword.png'
		},

		{
			s_ItemString = 'default:sword_stone',
			i_Damage = 4,
			s_Texture = 'default_tool_stonesword.png'
		},

		{
			s_ItemString = 'default:sword_bronze',
			i_Damage = 6,
			s_Texture = 'default_tool_bronzesword.png'
		},

		{
			s_ItemString = 'default:sword_steel',
			i_Damage = 6,
			s_Texture = 'default_tool_steelsword.png'
		},

		{
			s_ItemString = 'default:sword_mese',
			i_Damage = 7,
			s_Texture = 'default_tool_mesesword.png'
		},

		{
			s_ItemString = 'default:sword_diamond',
			i_Damage = 8,
			s_Texture = 'default_tool_diamondsword.png'
		}
	}



	if (a_b_RealisticChance ~= true) then
		t_ChosenSword = t_DefaultSwords[math.random(1, 6)]

	else
		--[[
			These percentages have been collected using Ores Stats
			Mapgen = flat
			Map seed = 0123456789
			Letting the character in autowalk from surface to -31000.

			The ore's percentages sum is 50.476%
			Stone and wood percentages have been arbitrarily set:
			(100% - 50.476%) = 49.524% -- To be assigned.
			(49.524% / 6) = 8.254% -- Allows the following:

			Stone's percentage = Twice the wood's percentage
			Stone is everywhere, wood might be harder to find.

			Stone = (8.254% * 4) = 33.016%
			Wood = (8.254% * 2) = 16.508%

			(33.016% + 16.508%) = 49.524% -- Assigned.

			Ores + (Wood + Stone) = 50.476% + 49.524% = 100% Assigned.
		--]]

		-- These values have been rounded to integers due the fact
		-- that the pseudorandom number generator only produces integers.

		local i_DIAMOND_ORE_PERCENTAGE = 2
		-- Actually 1.545

		local i_MESE_ORE_PERCENTAGE = 3
		-- Actually 2.818

		local i_COPPER_ORE_PERCENTAGE = 14
		-- Actually 13.796, used for 'bronze'.

		local i_WOOD_PERCENTAGE = 17
		-- Actually 16.508

		local i_IRON_ORE_PERCENTAGE = 32
		-- Actually 32.317, used for 'steel'.

		local i_STONE_PERCENTAGE = 33
		-- Actually 33.016

		i_RandomNumber = PseudoRandom(os.time())
		i_RandomNumber = i_RandomNumber:next(1, 100)

		if (i_RandomNumber <= i_DIAMOND_ORE_PERCENTAGE) then
			t_ChosenSword = t_DefaultSwords[6] -- Diamond sword.

		elseif (i_RandomNumber > i_DIAMOND_ORE_PERCENTAGE)
		and (i_RandomNumber <= i_MESE_ORE_PERCENTAGE)
		then
			t_ChosenSword = t_DefaultSwords[5] -- Mese sword.

		elseif (i_RandomNumber > i_MESE_ORE_PERCENTAGE)
		and (i_RandomNumber <= i_COPPER_ORE_PERCENTAGE)
		then
			t_ChosenSword = t_DefaultSwords[3] -- Copper sword.

		elseif (i_RandomNumber > i_COPPER_ORE_PERCENTAGE)
		and (i_RandomNumber <= i_WOOD_PERCENTAGE)
		then
			t_ChosenSword = t_DefaultSwords[1] -- Wooden sword.

		elseif (i_RandomNumber > i_WOOD_PERCENTAGE)
		and (i_RandomNumber <= i_IRON_ORE_PERCENTAGE)
		then
			t_ChosenSword = t_DefaultSwords[4] -- Steel sword.

		else
			t_ChosenSword = t_DefaultSwords[2] -- Stone sword.

		end
	end

	return t_ChosenSword
end
