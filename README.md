### MOBILES LIBRARY
**_Provides additional functions for LUA entities._**

**Version:** 0.1.0  
**Source code's license:** [EUPL v1.2][1] or later.  
**Texture license:** [CC-0 v1.0][2] or later.  
**Dependencies:** None. (See Note below.)  
**Optional dependencies:** [Intllib][3], [Mobs Redo][4]

__NOTE:__  
Some functions use Mobs Redo's variables, they may work with other mob APIs, but it is not guaranteed. See API.txt for a complete list.


### Installation

Unzip the archive, rename the folder to mobs_lib and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods


[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://creativecommons.org/share-your-work/public-domain/cc0/
[3]: https://forum.minetest.net/viewtopic.php?id=4929
[4]: https://forum.minetest.net/viewtopic.php?t=9917
