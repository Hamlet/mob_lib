--[[
	Mobiles Library - Provides functions for LUA entities.
	Copyright © 2019 Hamlet <hamlatmesehub@riseup.net> and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Global mod's namespace
--

mob_lib = {}


--
-- Variables
--

local s_ModPath = minetest.get_modpath("mob_lib")
local s_LogLevel = minetest.settings:get("debug_log_level")


--
-- Files' loaders
--

dofile(s_ModPath .. "/functions/characteristics.lua")
dofile(s_ModPath .. "/functions/extras.lua")
dofile(s_ModPath .. "/functions/server.lua")


--
-- Minetest engine debug logging
--

if (s_LogLevel == nil)
or (s_LogLevel == "action")
or (s_LogLevel == "info")
or (s_LogLevel == "verbose")
then
	s_LogLevel = nil
	minetest.log("action", "[Mod] Mobiles Library [0.1.0] loaded.")
end
